function contactform(form) {
	var mailtarget = place.contact.email1;
	var placename = place.title;
	var date = new Date(form.date.value);
	var datestring = date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear();

	var br = escape('\r\n');
	
	// TOOD somehow newlines without a character (space) in them aren't applied, escaping doesn't work at all
	var maillink = "mailto:"+mailtarget
		+"?subject=Mietanfrage%20Grillplatz%20"+placename
		+"&body=Guten%20Tag,"+br+br
		+"ich%20würde%20gerne%20den%20Grillplatz%20"+placename+"%20am%20"+datestring+"%20von%20"+form.time_begin.value+"%20bis%20"+form.time_end.value+"%20Uhr%20mieten."+br
		+"Sie%20können%20mich%20telefonisch%20unter%20"+form.phone.value+"%20oder%20per%20Mail%20an%20"+form.email.value+"%20erreichen."+br+br;

	if (form.message.value.length > 0) maillink += form.message.value+br+br;

	maillink += "Mit%20freundlichen%20Grüßen"+br+br
		+form.name.value+"%20"+form.surname.value+br+br
		+"(Grillplatz%20gefunden%20auf%20www.grillradar.de"+place.id+".html)";
	
	window.open(maillink, '_blank');
    return false;
}

// ---------------------------------------------------------------------------------------------------
// Form fields:
// ---------------------------------------------------------------------------------------------------
// form.name
// form.surname
// form.email
// form.phone
// form.date
// form.time_begin
// form.time_end
// form.message
// ---------------------------------------------------------------------------------------------------
// Email:
// ---------------------------------------------------------------------------------------------------
// Subject: Mietanfrage Grillplatz ____

// Guten Tag,

// ich würde gerne den Grillplatz ____ am form.date von form.time_begin bis form.time_end mieten.
// Sie können mich unter form.phone oder form.email erreichen.

// form.message

// Mit freundlichen Grüßen

// form.name form.surname

