//GLOBAL +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
var place = null;
//PLACE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function setPlace(){
    let path = window.location.pathname;
    for(let i=0;i<regions.length;i++){
        let places=regions[i].places;
        for(let j=0;j<places.length;j++){
            if(path.includes(places[j].id)){
				place = places[j];
                let meta="Grillplatz " +places[j].location+": "+places[j].short_description;
                document.getElementsByTagName('meta').namedItem('description')
                    .setAttribute('content',meta);
                document.getElementById("pageTitleGrillplatz").innerHTML= "Grillplatz "+places[j].location+" | "+places[j].short_description+" | Jetzt mieten";
                //HEADER
                document.getElementById('wallpaper').setAttribute("style","background-image: url("+places[j].wallpaper+")");
                document.getElementById('title').innerHTML=places[j].title;
                document.getElementById('location').innerHTML=places[j].location;

                //DESCRIPTION
                document.getElementById('description').innerHTML=places[j].description;


                //BULLETPOINTS
                document.getElementById('bulletpoints').innerHTML=places[j].bulletpoints[0]; //TODO iterate over bulletpoints

                //CONTACTINFO
                //document.getElementById('buttonLink').setAttribute("href",places[j].contact.buttonlink);
                document.getElementById('address1').innerHTML=places[j].contact.address1;
                document.getElementById('address2').innerHTML=places[j].contact.address2;
                document.getElementById('phone1').innerHTML=places[j].contact.phone1;
                document.getElementById('phone1').setAttribute("href","tel:"+places[j].contact.phone1);
                document.getElementById('phone1').setAttribute("title","telefon-grillplatz-"+places[j].location);
                document.getElementById('phone2').innerHTML=places[j].contact.phone2;
                document.getElementById('email1').innerHTML=places[j].contact.email1;
                document.getElementById('email2').innerHTML=places[j].contact.email2;
                document.getElementById('email1').setAttribute("href","mailto:"+places[j].contact.email1);
                document.getElementById('email1').setAttribute("title","mail-grillplatz-"+places[j].location);

                //RATING & RATING-DESCRIPTION
                document.getElementById('rating_insgesamt').innerHTML=places[j].ratings[0];
                //document.getElementById('desc_insgesamt').innerHTML=places[j].ratings_desc[0];

                document.getElementById('rating_sauberkeit').innerHTML=places[j].ratings[1];
               // document.getElementById('desc_sauberkeit').innerHTML=places[j].ratings_desc[1];

                document.getElementById('rating_erreichbarkeit').innerHTML=places[j].ratings[2];
                //document.getElementById('desc_erreichbarkeit').innerHTML=places[j].ratings_desc[2];

                document.getElementById('rating_kinderfreundlichkeit').innerHTML=places[j].ratings[3];
                //document.getElementById('desc_kinderfreundlichkeit').innerHTML=places[j].ratings_desc[3];

                document.getElementById('rating_atmosphaere').innerHTML=places[j].ratings[4];
               // document.getElementById('desc_atmosphaere').innerHTML=places[j].ratings_desc[4];

                document.getElementById('rating_ausstattung').innerHTML=places[j].ratings[5];
               // document.getElementById('desc_ausstattung').innerHTML=places[j].ratings_desc[5];

                // remove rent button if not needed
                if (places[j].contact.email1=='') {
		            document.getElementById('rentButton').remove(); 
	            }
                //IMAGES
                let content="";
                for(let l=0;l<places[j].images.length;l++){
                    content+='<div class="col-md-4"><a title="bild-grillplatz-'+places[j].location+'" itemprop="image" href="/img/places'+places[j].id+'/'+(l+1)+'.jpg" class="img-pop-up"><div class="single-gallery-image" style="background: url('+places[j].images[l]+');"></div></a></div>';
                }
                document.getElementById("images").innerHTML=content;


                //GMAP
                document.getElementById('gmap').setAttribute("src",places[j].gmaps_url);

                //weatherWidget
               document.getElementById('weatherWidget').setAttribute("href",places[j].weather);
               document.getElementById('weatherWidget').setAttribute("title","wetter-grillplatz-"+places[j].location);
               document.getElementById('weatherWidget').setAttribute("data-label_1",places[j].location);
            };
        }
    }
	if (place === null) {
		document.getElementById('location').innerHTML="Entfernt";
		document.getElementById('description').innerHTML="Dieser Grillplatz wurde leider von grillradar.de entfernt.";
	}
}

///////////////////////////////////////////// TEST /////////////////////////////////////////////////////////////////////
function initContact(index) {
    document.getElementById('address1').innerHTML=places[index].contact.address1;
    document.getElementById('address2').innerHTML=places[index].contact.address2;
    document.getElementById('phone1').innerHTML=places[index].contact.phone1;
    document.getElementById('phone2').innerHTML=places[index].contact.phone2;
    document.getElementById('email1').innerHTML=places[index].contact.email1;
    document.getElementById('email2').innerHTML=places[index].contact.email2;
}
function configPlace(j) {
    document.getElementById('description').innerHTML=this.places[j].description;
    document.getElementById('bulletpoints').innerHTML=this.places[j].bulletpoints[0];
    document.getElementById('buttonLink').setAttribute("href",this.places[j].contact.buttonlink);
}


//REGION ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}

function setAll() {
    let path = window.location.pathname;
	
    let meta="Die schönsten Grillplätze in der Ortenau | Grillplatz mieten für Kindergeburtstag, Grillfest, Klassenfest oder Betriebsfeier";
    document.getElementsByTagName('meta').namedItem('description')
        .setAttribute('content',meta);
    document.getElementById("pageTitleIndex").innerHTML="Die Besten Grillplätze der Ortenau | Grillplatz mieten für Kindergeburtstag, Grillfest, Klassenfest oder Betriebsfeier ";
    document.getElementById("regionName").innerHTML="der Ortenau";
    document.getElementById("regionNameText").innerHTML="der Ortenau"

    for(let i=0;i<regions.length;i++){
        let favorites = regions[i].favorites;
		shuffle(favorites);
        let topPlaces="";
        for(let j=0;j<favorites.length;j++){
            //IMAGES
            topPlaces+='<div class="single_slider d-flex align-items-center overlay" style="background-image: url('+favorites[j].thumbnail+')"> <div class="container"> ' +
                '<div class="row align-items-center justify-content-center"> ' +
                '<div class="col-xl-9 col-md-9 col-md-12"> <div class="slider_text text-center"> ' +
                '<div class="deal_text"> <span>Topplatz</span> </div> ' +
                '<h3><strong>'+favorites[j].title+'</strong></h3> <h2>'+favorites[j].location+'</h2>' +
                '<a title="grillplatz-'+favorites[j].location+'" href="'+favorites[j].id+'" class="boxed-btn3">Anzeigen</a>' +
                '</div> </div> </div> </div>' +
				'<i class="fa fa-chevron-down scrolldown" onclick="scrollDown()"></i></div>';
        };
        document.getElementById("topPlaces").innerHTML+=topPlaces;
        let content="";
        let places=regions[i].places;
        for(let k=0;k<places.length;k++){
                //IMAGES
            let rating =places[k].ratings[0];
            content+= '<div class="single_Burger_President"><div class="room_thumb">' +
                '<img src="/img/places'+places[k].id+'/thumbnail.jpg" alt="Das Thumbnail vom Grillplatz in'+places[k].location+'">' +
                '<div class="room_heading d-flex justify-content-between align-items-center">' +
                '<div class="room_heading_inner">' +
                '<span><strong>'+places[k].title+'</strong></span>' +
                '<h3>'+places[k].location+'</h3>' +
                '<span class="rating">'+rating+'</span>'+
                '<p>'+places[k].short_description+'</p>' +
                '<a title="grillplatz-'+places[k].location+'-anzeigen" href="https://grillradar.de'+places[k].id+'.html" class="boxed-btn3">Anzeigen</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
				'</div>';
        };
        document.getElementById("places").innerHTML+=content;
    }
}

function setRegion() {
    let path = window.location.pathname;
    if(path.includes("/grillplatz/index.html")) {
		setAll();
		return; // sorry!
	}
    for(let i=0;i<regions.length;i++){
        if(path.includes(regions[i].id)){
            let meta="Die schönsten Grillplätze in "+regions[i].title+"| Grillplatz mieten für Kindergeburtstag, Grillfest, Klassenfest oder Betriebsfeier";
            document.getElementsByTagName('meta').namedItem('description')
                .setAttribute('content',meta);
            document.getElementById("pageTitleIndex").innerHTML="Die Besten Grillplätze | "+regions[i].title+" | Grillplatz mieten für Kindergeburtstag, Grillfest, Klassenfest oder Betriebsfeier ";
        let favorites = regions[i].favorites;
		shuffle(favorites);
        let topPlaces="";
            for(let j=0;j<favorites.length;j++){
                //IMAGES
                topPlaces+='<div class="single_slider  d-flex align-items-center overlay" style="background-image: url('+favorites[j].thumbnail+')"> <div class="container"> ' +
                    '<div class="row align-items-center justify-content-center"> ' +
                    '<div class="col-xl-9 col-md-9 col-md-12"> <div class="slider_text text-center"> ' +
                    '<div class="deal_text"> <span>Topplatz</span> </div> ' +
                    '<h3><strong>'+favorites[j].title+'</strong></h3> <h2>'+favorites[j].location+'</h2>' +
                	'<a title="grillplatz-'+favorites[j].location+'" href="'+favorites[j].id+'" class="boxed-btn3">Anzeigen</a>' +
                    '</div> </div> </div> </div>' +
					'<i class="fa fa-chevron-down scrolldown" onclick="scrollDown()"></i></div>';
            };
            document.getElementById("topPlaces").innerHTML=topPlaces;
            let content="";
            let places=regions[i].places;
        for(let k=0;k<places.length;k++){
                //IMAGES
            let rating =places[k].ratings[0];
            content+= '<div class="single_Burger_President"><div class="room_thumb">' +
                '<img src="/img/places'+places[k].id+'/thumbnail.jpg" alt="Das Thumbnail vom Grillplatz in'+places[k].location+'">' +
                '<div class="room_heading d-flex justify-content-between align-items-center">' +
                '<div class="room_heading_inner">' +
                '<span><strong>'+places[k].title+'</strong></span>' +
                '<h3>'+places[k].location+'</h3>' +
                '<span class="rating">'+rating+'</span>'+
                '<p>'+places[k].short_description+'</p>' +
                '<a title="grillplatz-'+places[k].location+'-anzeigen" href="https://grillradar.de'+places[k].id+'.html" class="boxed-btn3">Anzeigen</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
				'</div>';
            };
            document.getElementById("places").innerHTML=content;

            document.getElementById("regionName").innerHTML=regions[i].title;
            document.getElementById("regionNameText").innerHTML=regions[i].title;
        }

    }
}

function setOrtenau(){
    document.getElementById("nmbOffenburg").innerHTML=(regions[0].places.length)+" Grillplätze";
    document.getElementById("nmbKehl").innerHTML=(regions[4].places.length)+" Grillplätze";
    document.getElementById("nmbAchern").innerHTML=(regions[3].places.length)+" Grillplätze";
    document.getElementById("nmbLahr").innerHTML=(regions[2].places.length)+" Grillplätze";
    document.getElementById("nmbKinzigtal").innerHTML=(regions[1].places.length)+" Grillplätze";
}


//REGION ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function setBest() {
    let path = window.location.pathname;
    for(let i=0;i<ratingBest.length;i++) {
        if (path.includes(ratingBest[i].id)) {
            let meta = ratingBest[i].title + "| Grillplatz mieten für Kindergeburtstag, Grillfest, Klassenfest oder Betriebsfeier";
            document.getElementsByTagName('meta').namedItem('description')
                .setAttribute('content', meta);
            document.getElementById("pageTitleIndex").innerHTML = ratingBest[i].title + " | Grillplatz mieten für Kindergeburtstag, Grillfest, Klassenfest oder Betriebsfeier ";
            let favorites = ratingBest[i].favorites;
            shuffle(favorites);
            let topPlaces = "";
            for (let j = 0; j < favorites.length; j++) {
                //IMAGES
                topPlaces += '<div class="single_slider  d-flex align-items-center overlay" style="background-image: url(' + favorites[j].thumbnail + ')"> <div class="container"> ' +
                    '<div class="row align-items-center justify-content-center"> ' +
                    '<div class="col-xl-9 col-md-9 col-md-12"> <div class="slider_text text-center"> ' +
                    '<div class="deal_text"> <span>Topplatz</span> </div> ' +
                    '<h3><strong>'+ favorites[j].title + '</strong></h3> <h2>' + favorites[j].location + '</h2>' +
                	'<a title="grillplatz-'+favorites[j].location+'" href="'+favorites[j].id+'" class="boxed-btn3">Anzeigen</a>' +
                    '</div> </div> </div> </div>' +
					'<i class="fa fa-chevron-down scrolldown" onclick="scrollDown()"></i></div>';
            }
            ;
            document.getElementById("topPlaces").innerHTML = topPlaces;
            let content = "";
            let places = ratingBest[i].places;
            for (let k = 0; k < places.length; k++) {
                //IMAGES
                let rating = places[k].ratings[0];
                content += '<div class="single_Burger_President"><div class="room_thumb">' +
                    '<img src="/img/places' + places[k].id + '/thumbnail.jpg" alt="Das Thumbnail vom Grillplatz in' + places[k].location + '">' +
                    '<div class="room_heading d-flex justify-content-between align-items-center">' +
                    '<div class="room_heading_inner">' +
                    '<span><strong>' + (k+1) + ". " + places[k].title + '</strong></span>' +
                    '<h3>' + places[k].location + '</h3>' +
                    '<span class="rating">' + rating + '</span>' +
                    '<p>' + places[k].short_description + '</p>' +
                    '<a title="grillplatz-' + places[k].location + '-anzeigen" href="https://grillradar.de' + places[k].id + '.html" class="boxed-btn3">Anzeigen</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
					'</div>';
            }
            ;
            document.getElementById("places").innerHTML = content;

            document.getElementById("regionName").innerHTML = ratingBest[i].title;
            // document.getElementById("regionNameText").innerHTML=ratingBest[i].title;
            document.getElementById("regionDesc").innerHTML = ratingBest[i].description;

        }
    }
}
