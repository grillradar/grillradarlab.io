function searchPLZ(form) {
	for (var i = 0; i < regions.length; i++) {
		var region = regions[i];
		if (region.plzs.includes(parseInt(form.plz.value))) {
			window.location = "https://www.grillradar.de/grillplatz" + region.id;
			return true;
		}
	}
	alert("Leider haben wir für die PLZ " + form.plz.value + " keine Grillplätze.");
    return false;
}

function searchCategory(form) {
	window.location = "https://www.grillradar.de/grillplatz/die-besten/" + form.category.value + ".html";
    return true;
}
