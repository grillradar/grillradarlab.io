
/* AJAX SEARCH */
function searchToubizEventsAjax ( date_to, highlight, category, location, offset )
{
	$(".lisevents_pagination").html('<img src="/extension/lisevents/design/standard/images/loader/loader_16.png" alt="loader"/>');
	var uri = '/lisevents/load/' + date_to + '/' + highlight + '/' + category + '/' + location + '/' + offset;
	
	$.ajax({
  		url: uri,
  		context: "",
		success: function(data) {
			$(".lisevents_pagination").hide();	
				
			if (data['count'] == 0) {
				$('#lisevents_result').html('');
				$('#lisevents_result_false').html('Es wurden keine Veranstaltungen gefunden.');
				$('#lisevents_result_false').show();
			} else {
				$('#lisevents_result').append(data['html']);
				$('#lisevents_result_true').html('Es wurden ' + data['count'] + ' Veranstaltungen gefunden.');
				$('#lisevents_result_true').show();				
			}
			
			//Mobile Dom Refresh
			if ($('.full_children_list').length)
				$('.full_children_list').listview('refresh').trigger('create');				    
		}
	});	

	return false;
}

function searchToubizEventsForm (type)
{
	$('#lisevents_result').html('');
	$('#lisevents_result_false').hide();
	$('#lisevents_result_true').hide();	
	$('#lisevents_pagination_top').show();

	var date_to = "";
	var highlight = 0;
	var category = "";
	var location = "";
	var offset = 0;		
	
	if ( typeof $('#date_to').val() !== "undefined" && $('#date_to').val()) {	
		date_to = $("#date_to").val();
	}

	if ( typeof $('#location').val() !== "undefined" && $('#location').val()) {	
		location = $("#location").val();
	}

	if ( typeof $('#category').val() !== "undefined" && $('#category').val()) {	
		category = $("#category").val();	
	}
	
	if ($('#highlight').is(':checked')) {
		highlight = 1;	
	}
	
	searchToubizEventsAjax ( date_to, highlight, category, location, offset );
	
	if (type == "mobile") {
		$('.ui-dialog').dialog('close');
	}
	
	return false;
}

