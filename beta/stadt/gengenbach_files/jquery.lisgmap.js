function lisgmap_init(selector, lis_latitude, lis_longitude, lis_zoom, lis_markers) {

	var map_latitude  = 0;
	var map_longitude = 0;
	var map_zoom      = 10;

	if (lis_latitude != "")
		map_latitude = lis_latitude;

	if (lis_longitude != "")
		map_longitude = lis_longitude;

	if (lis_zoom != "")
		map_zoom = lis_zoom;

	if (lis_markers == "")
	{
		var lismap = $(selector).gMap({
				alpmap: false,
				latitude: map_latitude,
				longitude: map_longitude,
				zoom: map_zoom
		});
	}
	else
	{
		var lismap = $(selector).gMap({
				alpmap: false,
				latitude: map_latitude,
				longitude: map_longitude,
				zoom: map_zoom,
				markers: lis_markers
		});
	}

	return lismap;
}

/**
 * jQuery gMap v3
 *
 * @url         http://www.smashinglabs.pl/gmap
 * @author      Sebastian Poreba <sebastian.poreba@gmail.com>
 * @version     3.2.0
 * @date        19.08.2011
**/
(function ($) {
    "use strict";

    // global google maps objects
    var $googlemaps = google.maps,
    	oms,
    	markerClusterer = null,
    	markerAll = [],
    	markerShowTrue = [],
    	markerShowFalse = [],
        markerGroups = {},
        polylineAll = [],
        mainInfoWindow,
        $geocoder = new $googlemaps.Geocoder(),
        opts = {},
        $markersToLoad = 0,
        methods = {}; // for JSLint
    methods = {
        init: function (options) {
            var k,
            // Build main options before element iteration
            opts = $.extend({}, $.fn.gMap.defaults, options);

            // recover icon array
            for (k in $.fn.gMap.defaults.icon) {
                if(!opts.icon[k]) {
                    opts.icon[k] = $.fn.gMap.defaults.icon[k];
                }
            }

            // Iterate through each element
            return this.each(function () {

                var $this = $(this),
                    i, $data;

                if (opts.zoom == "fit") {
                    opts.zoom = methods.autoZoom.apply($this, [opts]);
                }

                var mapOptions = {
                        zoom: opts.zoom,
                        mapTypeControl: opts.mapTypeControl,
                        zoomControl: opts.zoomControl,
                        panControl : opts.panControl,
                        scaleControl : opts.scaleControl,
                        streetViewControl: opts.streetViewControl,
                        mapTypeId: opts.maptype,
                        mapTypeIds: opts.maptypeids,
						mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU, position: google.maps.ControlPosition.TOP_LEFT },
                        scrollwheel: opts.scrollwheel,
                        maxZoom: opts.maxZoom,
                        minZoom: opts.minZoom
                    };

                    // Create map and set initial options
                    if (opts.alpmap == false) {
                    	var $gmap = new $googlemaps.Map(this, mapOptions);
                    } else {
	                    var mapTypeIds = ['alpstein_map', 'alpstein_hybrid', 'alpstein_map_winter', $googlemaps.MapTypeId.SATELLITE, $googlemaps.MapTypeId.TERRAIN];
	                    var $gmap = new alp.gmap3.Map(document.getElementById("lisgmap_map"), {
	                           zoom : opts.zoom,
	                           mapTypeId : mapTypeIds[ 0 ],
	                           mapTypeControlOptions : { mapTypeIds: mapTypeIds, style: google.maps.MapTypeControlStyle.DROPDOWN_MENU, position: google.maps.ControlPosition.TOP_LEFT }
	                    });
                    }

                    opts.spider = new OverlappingMarkerSpiderfier($gmap, {markersWontMove: false, markersWontHide: false, keepSpiderfied: true, nearbyDistance: 20, legWeight: 1.5});

                // Create map and set initial options
                $this.data("$gmap", $gmap);

                $this.data('gmap', {
                    'opts': opts,
                    'gmap': $gmap,
                    'methods': methods,
                    'markers': [],
                    'markerKeys' : {}
                });

                // Check for map controls
                if (opts.controls.length !== 0) {
                    for (i = 0; i < opts.controls.length; i += 1) {
                        $gmap.controls[opts.controls[i].pos].push(opts.controls[i].div);
                    }
                }

                //LIS EXTENSION
                if (opts.kml != "") {
                	var georssLayer = new google.maps.KmlLayer(opts.kml);
					georssLayer.setMap($gmap);
                }

				$(document)
					.ajaxStart(function() {
						$('#lisgmap_container_loader').show();
						$('.lisgmap_trigger').addClass('lisgmap_trigger_loading');
					})
					.ajaxStop(function(){
						$('#lisgmap_container_loader').hide();
						$('.lisgmap_trigger').removeClass('lisgmap_trigger_loading');
					});

                //-> Handler fetch marker from listen, folder etc...
				$('a.lisgmap_nav_list_link').click(function(e) {

					// Gruppe mit Points prüfen und ggf erstellen
					var group 	= "group_"+$(this).attr('rel');
					var offset 	= "";
					var limit 	= "";
					var uri 	= opts.baseuri + '/' + $(this).attr('rel') + "/" + opts.client;

					if ( typeof $(this).attr('offset') !== "undefined" && $(this).attr('offset') != "") {
						offset = $(this).attr('offset');
					}
					if ( typeof $(this).attr('limit') !== "undefined" && $(this).attr('limit') != "") {
						limit = $(this).attr('limit');
					}

					if (opts.log) {console.log("toggle group: " + group)};

					uri = uri + '/' + offset + '/' + limit;

					if (typeof markerGroups[group] == "undefined") {
						markerGroups[group] = [];
						$.ajax({
							  url: uri,
							  dataType: 'json',
							  async: true,
							  success: function(data) {
 							  	if (opts.log) {console.log(data)};
 							  	methods.addMarkers.apply($this, [data]);

			                    // generierte marker den opts.marker hinzugfügen
								for (var i = 0; i < data.length; i+= 1) {
									opts.markers.push(data[i]);
                				}

                				methods.lisBoundsAndCluster($gmap, opts);
							  }
						});

					} else {
						methods.lisToggleGroup(group, $this);
					}

					if ($(this).hasClass('active')) {
						$(this).removeClass('active');
					} else {
						$(this).addClass('active');
					}

					e.preventDefault();
					return false;

				});

				//-> Handler für virtuelle Listen/Folder etc...
				$('a.lisgmap_nav_list_link_virtual').click(function(e) {

					// Gruppe mit Points prüfen und ggf erstellen
					var group = "group_" + $(this).attr('rel');

					if (opts.log){
						console.log("data-virtual" + $(this).attr('data-virtual'));
						console.log("toggle virtual group: " + group);
					}

					if (typeof markerGroups[group] == "undefined") {

						if ($(this).attr('data-virtual') != "")
						{
							markerGroups[group] = [];
							$.ajax({
								  url: opts.baseuri_virtual + '/' + opts.client,
								  dataType: 'json',
								  //MUSS POST SEIN WEGEN suhosin
								  type: 'POST',
								  data: { 'virtual': $(this).attr('data-virtual') },
								  async: true,
								  success: function(data) {
	 							  	if (opts.log) {console.log(data)};

				                    methods.addMarkers.apply($this, [data]);

				                    // generierte marker den opts.marker hinzugfügen
									for (var i = 0; i < data.length; i+= 1) {
										opts.markers.push(data[i]);
	                				}

	                				methods.lisBoundsAndCluster($gmap, opts);

								  }
							});
						}

					} else {
						methods.lisToggleGroup(group, $this);
					}

					if ($(this).hasClass('active')) {
						$(this).removeClass('active');
					} else {
						$(this).addClass('active');
					}

					e.preventDefault();
					return false;

				});


                //-> Toggle Fullscreen
			  	$(".switch_fullscreen").click(function(e){
			  		var target = $(this).attr('data-target');
			  		if ($(target).hasClass('fullscreen_on'))
			  		{
				  		$(target).removeClass('fullscreen_on');
				  		$(target).addClass('fullscreen_off');
				  		$(this).html('<i class="icon-resize-full icon-white"></i>');
			  		}
			  		else
			  		{
				  		$(target).removeClass('fullscreen_off');
				  		$(target).addClass('fullscreen_on');
				  		$(this).html('<i class="icon-resize-small icon-white"></i>');
			  		}

					methods.fixAfterResize.apply($this, [opts]);
					methods.lisBoundsAndCluster($gmap, opts);

					e.preventDefault();
					return false;
			  	});


				var first_load = 0;
				$("a.lisgmap_nav_list_link").each(function() {
					if ($(this).attr('rev') == 1) {
						first_load = 1;
						$(this).click();
					}
				});
				$("a.lisgmap_nav_list_link_virtual").each(function() {
					if ($(this).attr('rev') == 1) {
						first_load = 1;
						$(this).click();
					} else if ($(this).attr('rev') == 2) {
						first_load = 2;
					}
				});
				if (first_load == 0) {
					// erster Menupunkt klicken und Points laden
					$('a.lisgmap_nav_list_link:first').click();
					$('a.lisgmap_nav_list_link_virtual:first').click();
				}

				// Options Markers
				if (opts.markers.length !== 0) {
                    methods.addMarkers.apply($this, [opts.markers]);
                    // Bounding Refresh -> Nur wenn mehr als 1 Marker!
                    if (opts.markers.length > 1) {
						methods.lisBoundsAndCluster($gmap, opts);
					} else {
						var centerpoint = new $googlemaps.LatLng(opts.markers[0].latitude, opts.markers[0].longitude);
						$gmap.setCenter(centerpoint,opts.zoom);
					}
                }

				$(window).resize(function(){
					methods.fixAfterResize.apply($this, [opts]);
					methods.lisBoundsAndCluster($gmap, opts);
				});

				methods.lisOverlay($gmap);
                methods._onComplete.apply($this, []);
            });
        },

        lisSpiderMarker: function(map, opts)
        {
	        opts.spider.addListener('click', function(marker) {
	        	if (opts.singleInfoWindow && mainInfoWindow) {
	        		mainInfoWindow.close();
	        	}
	        	var lisWindow = methods.lisInfoWindow(map, marker, opts);
  				lisWindow.open(map, marker);
  				mainInfoWindow = lisWindow;
			});
			opts.spider.addListener('spiderfy', function(markers) {
				// for(var i = 0; i < markers.length; i ++) {
				// 	markers[i].setIcon(iconWithColor(spiderfiedColor));
				// 	markers[i].setShadow(null);
				// }
				if (mainInfoWindow)
					mainInfoWindow.close();
			});
			opts.spider.addListener('unspiderfy', function(markers) {
				// for(var i = 0; i < markers.length; i ++) {
				// 	markers[i].setIcon(iconWithColor(usualColor));
				// 	markers[i].setShadow(shadow);
				// }
				if (mainInfoWindow)
					mainInfoWindow.close();
			});
        },

        lisInfoWindow: function(map, marker, opts)
        {
			var lisWindowOptions = {
				content: marker.html,
				pixelOffset: new google.maps.Size(-110, -30),
				boxClass: "lisgmap_marker_window",
				closeBoxMargin: "-10px",
				closeBoxURL: opts.img_close_icon,
				enableEventPropagation: false,
				alignBottom: true
	        };
	        var lisWindow = new InfoBox(lisWindowOptions);

			$googlemaps.event.addListener(marker, "click", function() {
				if (opts.singleInfoWindow && mainInfoWindow) {mainInfoWindow.close();}
			    var lisWindow = new InfoBox(lisWindowOptions);
			    lisWindow.open(map, marker);
				mainInfoWindow = lisWindow;
			});

			if (marker.popup)
				lisWindow.open(map, marker);

			// Window bei Click auf Karte ausblenden
			$googlemaps.event.addListener(map, 'click', function() {
 				mainInfoWindow.close();
		    });

	        return lisWindow;
        },

        lisBoundsAndCluster: function(map, opts)
        {
       		methods.lisGetMarkers();

        	if (opts.bounding == true)
        	{
	        	var mylisbounds = new $googlemaps.LatLngBounds();

	        	if (markerShowTrue.length > 0)
	        	{
		        	for (var i = 0; i < markerShowTrue.length; i++)
		        	{
		        		mylisbounds.extend(markerShowTrue[i].getPosition());
		        	}
	        	}
	        	else
	        	{
                    var center = new $googlemaps.LatLng(opts.latitude, opts.longitude);
	        		mylisbounds.extend(center);
	        	}

	        	map.setCenter(mylisbounds.getCenter());
				map.fitBounds(mylisbounds);
        	}

        	if (opts.clustering == true)
        	{
		        // Erst Cluster checken und löschen
	        	if (markerClusterer) {
		        	methods.lisHidePolylines.apply(this, [opts]);
		        	markerClusterer.clearMarkers();
		        }

		        var mcOptions = {
		        	gridSize: 50,
		        	maxZoom: 14,
		        	styles: [{
		        		// textColor: 'white',
						height: 53,
						url: "/extension/lisgmap/design/standard/images/m1.png",
						width: 53
						},
						{
						height: 53,
						url: "/extension/lisgmap/design/standard/images/m1.png",
						width: 53
						},
						{
						height: 53,
						url: "/extension/lisgmap/design/standard/images/m1.png",
						width: 53
						},
						{
						height: 53,
						url: "/extension/lisgmap/design/standard/images/m1.png",
						width: 53
						},
						{
						height: 53,
						url: "/extension/lisgmap/design/standard/images/m1.png",
						width: 53
						}
					]
		        };

	       		markerClusterer = new MarkerClusterer(map, markerShowTrue, mcOptions);
        	}

        	if (opts.spiderfy == true)
        	{
       			methods.lisSpiderMarker(map, opts);
       		}
       		else
       		{
				for (var i = 0; i < markerAll.length; i++)
		        {
		        	methods.lisInfoWindow(map, markerAll[i], opts);
		        }
       		}
        },

        lisGetMarkers: function() {
	        // Sichtbare und nicht sichtbare Marker Arrays neu speichern
			markerShowTrue  = [];
			markerShowFalse = [];
			markerAll       = [];

	        // Alle sichtbaren Marker zum Clustering hinzufügen
        	for (var group in markerGroups)
        	{
				for (var i = 0; i < markerGroups[group].length; i++)
				{
					var marker = markerGroups[group][i];
					markerAll.push(marker);
					if (marker.getVisible()) {
						markerShowTrue.push(marker);
					} else {
						markerShowFalse.push(marker);
					}

				}
        	}
        },

        lisToggleGroup: function (group, map) {

			var $data = map.data('gmap'),
				$gmap = $data.gmap;

			for (var i = 0; i < markerGroups[group].length; i++) {
				var marker = markerGroups[group][i];

				if (marker.getVisible()) {
					marker.setVisible(false);
					if (marker.polyline)
						marker.polyline.setMap(null);
				} else {
					marker.setVisible(true);
					if (marker.polyline)
						marker.polyline.setMap($gmap);
				}
			}

			methods.lisBoundsAndCluster($gmap, $data.opts);
			return false;
        },

        lisHidePolylines: function () {
			for (var i = 0; i < polylineAll.length; i++) {
				var lispolyline = polylineAll[i];
				lispolyline.setMap(null);
			}
        },

        _onComplete: function () {
            var $data = this.data('gmap'),
                that = this;
            if ($markersToLoad !== 0) {
                window.setTimeout(function () {methods._onComplete.apply(that, []); }, 1000);
                return;
            }

            $data.opts.onComplete();
        },

        processMarker: function (marker, gicon, gshadow, location) {
            var $data = this.data('gmap'),
                $gmap = $data.gmap,
                opts = $data.opts,
                gmarker,
                markeropts;

            if (location === undefined) {
                location = new $googlemaps.LatLng(marker.latitude, marker.longitude);
            }

            if (!gicon) {
                // Set icon properties from global options
                var _gicon = {
                    image: opts.icon.image,
                    iconSize: new $googlemaps.Size(opts.icon.iconsize[0], opts.icon.iconsize[1]),
                    iconAnchor: new $googlemaps.Point(opts.icon.iconanchor[0], opts.icon.iconanchor[1]),
                    infoWindowAnchor: new $googlemaps.Size(opts.icon.infowindowanchor[0], opts.icon.infowindowanchor[1])
                };
                gicon = new $googlemaps.MarkerImage(_gicon.image, _gicon.iconSize, null, _gicon.iconAnchor);
            }

            markeropts = {
                    position: location,
                    icon: gicon,
                    title: marker.title,
                    map: $gmap
                };


            gmarker = new $googlemaps.Marker(markeropts);
            $data.markers.push(gmarker);
            markerShowTrue.push(gmarker);

            if(marker.key) {$data.markerKeys[marker.key] = gmarker; }
            
            // outdooractive tracking
			if (marker.oaid) {
			  gmarker.addListener('click', function() {
			    event.stopPropagation();
			
			     //id muss ein string sein!
			     var tid = marker.oaid.toString();
			       oax.api.trackTeaser(tid);
			  });
			}

			// Window öffnen
			var markerContent = document.createElement("div");
	        markerContent.innerHTML = '<div class="lisgmap_marker">' + marker.html + '</div><div class="lisgmap_marker_arrow"></div>';
			gmarker.html =  markerContent;
			gmarker.popup = marker.popup;

			// Marker PopUp gleich anzeigen
    //         if (marker.popup) {
		  //       var lisWindow = new InfoBox(lisWindowOptions);
    //             lisWindow.open($gmap, gmarker);
				// $data.lisWindow = lisWindow;
    //         }

            //KML vorhanden (Toubiz Touren)
            if (marker.kml) {
				var georssLayer = new google.maps.KmlLayer(marker.kml);
            	$googlemaps.event.addListener(gmarker, 'mouseover', function() {
					georssLayer.setMap($gmap);
				});
				//Tour ausblenden
	            $googlemaps.event.addListener(gmarker, 'mouseout', function() {
	            	georssLayer.setMap(null);
	            });
            }

            //Geometry Daten vorhanden (Alpstein Touren)
            if (marker.geometry) {
	            // Tour anzeigen
				var pointAllArray = marker.geometry.split(" ");
				var tourCoordinates = [];

				for (var i = 0; i < pointAllArray.length; i++) {
					var pointArray = pointAllArray[i].split(",");
					var point = new $googlemaps.LatLng(pointArray[1], pointArray[0]);
					tourCoordinates.push(point);
				}

				var tourPath = new $googlemaps.Polyline({
    				path: tourCoordinates,
					strokeColor: "#ac1512",
    				strokeOpacity: 0.9,
					strokeWeight: 4
				});

				// Alle Polylines ins Array schreiben, um damit Sachen anzustellen
				polylineAll.push(tourPath);

	            if (marker.geometry_show == 0) {

	            	$googlemaps.event.addListener(gmarker, 'mouseover', function() {

						tourPath.setMap($gmap);

			            //Tour ausblenden
			            $googlemaps.event.addListener(gmarker, 'mouseout', function() {
			            	tourPath.setMap(null);
			            });
			            // Alternativ alle Touren ausblenden
   	            		// methods.lisHidePolylines.apply(this, [opts]);

		            });
	            } else {
					tourPath.setMap($gmap);
					$.extend(gmarker, {polyline:tourPath});
	            }

            }

            if (marker.group) {
            	//erstelle MarkerGruppe wenn nicht vorhanden
            	if (!markerGroups[marker.group]) {
					markerGroups[marker.group] = [];
            	}
				markerGroups[marker.group].push(gmarker);
			} else {
				var randomgroup = Math.random().toString(36).substring(7);
				markerGroups[randomgroup] = [];
				markerGroups[randomgroup].push(gmarker);
			}

			// Marker zu Spider hinzufügen
			if (opts.spiderfy)
				opts.spider.addMarker(gmarker);
        },

        addMarkers: function (markers){
            var opts = this.data('gmap').opts;

            if (markers.length !== 0) {
                if (opts.log) {console.log("adding " + markers.length +" markers");}
                // Loop through marker array
                for (var i = 0; i < markers.length; i+= 1) {
                    methods.addMarker.apply($(this),[markers[i]]);
                }
            }
            return this;
        },

        addMarker: function (marker) {

            var opts = this.data('gmap').opts;

            if (opts.log) {console.log("putting marker at " + marker.latitude + ', ' + marker.longitude + " with address " + marker.address + " and html "  + marker.html); }

            // Create new icon
            // Set icon properties from global options
            var _gicon = {
                image: opts.icon.image,
                iconSize: new $googlemaps.Size(opts.icon.iconsize[0], opts.icon.iconsize[1]),
                iconAnchor: new $googlemaps.Point(opts.icon.iconanchor[0], opts.icon.iconanchor[1]),
                infoWindowAnchor: new $googlemaps.Size(opts.icon.infowindowanchor[0], opts.icon.infowindowanchor[1])
            }

            // not very nice, but useful
            marker.infoWindowAnchor = _gicon.infoWindowAnchor;

            if (marker.icon) {
                // Overwrite global options
                if (marker.icon.image) { _gicon.image = marker.icon.image; }
                if (marker.icon.iconsize) { _gicon.iconSize = new $googlemaps.Size(marker.icon.iconsize[0], marker.icon.iconsize[1]); }

                if (marker.icon.iconanchor) { _gicon.iconAnchor = new $googlemaps.Point(marker.icon.iconanchor[0], marker.icon.iconanchor[1]); }
                if (marker.icon.infowindowanchor) { _gicon.infoWindowAnchor = new $googlemaps.Size(marker.icon.infowindowanchor[0], marker.icon.infowindowanchor[1]); }
            }

            var gicon = new $googlemaps.MarkerImage(_gicon.image, _gicon.iconSize, null, _gicon.iconAnchor);
			var gshadow;

            // Check if address is available
            if (marker.address) {
                // Check for reference to the marker's address
                if (marker.html === '_address') {
                    marker.html = marker.address;
                }

                if (marker.title == '_address') {
                    marker.title = marker.address;
                }

                if (opts.log) {console.log('geocoding marker: ' + marker.address); }
                // Get the point for given address
                methods._geocodeMarker.apply(this, [marker, gicon, gshadow]);
            } else {
                // Check for reference to the marker's latitude/longitude
                if (marker.html === '_latlng') {
                    marker.html = marker.latitude + ', ' + marker.longitude;
                }

                if (marker.title == '_latlng') {
                    marker.title = marker.latitude + ', ' + marker.longitude;
                }

               // Create marker
                var gpoint = new $googlemaps.LatLng(marker.latitude, marker.longitude);
                methods.processMarker.apply(this, [marker, gicon, gshadow, gpoint]);
            }
            return this;
        },


        fixAfterResize: function () {
            var data = this.data('gmap');
            $googlemaps.event.trigger(data.gmap, 'resize');
        },

        getMarker: function (key) {
            return this.data('gmap').markerKeys[key];
        },

		lisOverlay: function(map) {

	        // gernerate the hole on http://www.birdtheme.org/useful/v3tool.html
		    var latExtent = 86;
		    var lngExtent = 180;
		    var lngExtent2 = lngExtent - 1e-10;

		    //First ist Dark World / Second the hole
		    var paths = [[
		        new $googlemaps.LatLng(-latExtent, -lngExtent),
		        new $googlemaps.LatLng(latExtent, -lngExtent),
		        new $googlemaps.LatLng(latExtent, 0),
		        new $googlemaps.LatLng(-latExtent, 0),
		        new $googlemaps.LatLng(-latExtent, -lngExtent),
		        new $googlemaps.LatLng(-latExtent, lngExtent2),
		        new $googlemaps.LatLng(latExtent, lngExtent2),
		        new $googlemaps.LatLng(latExtent, 0),
		        new $googlemaps.LatLng(-latExtent, 0),
		        new $googlemaps.LatLng(-latExtent, lngExtent2)
		    ], [
				new $googlemaps.LatLng(47.663306390027024, 8.109283447265625),
				new $googlemaps.LatLng(47.666543412869466, 8.101387023925781),
				new $googlemaps.LatLng(47.67868046074391, 8.107223510742188),
				new $googlemaps.LatLng(47.68734805244351, 8.114948272705078),
				new $googlemaps.LatLng(47.68931250635065, 8.10842514038086),
				new $googlemaps.LatLng(47.697516190510626, 8.102588653564453),
				new $googlemaps.LatLng(47.70514099299213, 8.104305267333984),
				new $googlemaps.LatLng(47.70860644364875, 8.100528717041016),
				new $googlemaps.LatLng(47.715074668416335, 8.094348907470703),
				new $googlemaps.LatLng(47.71969433744451, 8.095035552978516),
				new $googlemaps.LatLng(47.73054894741148, 8.09640884399414),
				new $googlemaps.LatLng(47.73355088699629, 8.11065673828125),
				new $googlemaps.LatLng(47.738630698233415, 8.110313415527344),
				new $googlemaps.LatLng(47.73886158696909, 8.120098114013672),
				new $googlemaps.LatLng(47.734820886266306, 8.129711151123047),
				new $googlemaps.LatLng(47.73828436321007, 8.138980865478516),
				new $googlemaps.LatLng(47.747057473720666, 8.13863754272461),
				new $googlemaps.LatLng(47.750981807646866, 8.134174346923828),
				new $googlemaps.LatLng(47.7540979796801, 8.117866516113281),
				new $googlemaps.LatLng(47.750173880733605, 8.109111785888672),
				new $googlemaps.LatLng(47.749481361968, 8.095893859863281),
				new $googlemaps.LatLng(47.756867753724954, 8.094863891601562),
				new $googlemaps.LatLng(47.759752778243886, 8.084564208984375),
				new $googlemaps.LatLng(47.765291576877416, 8.068771362304688),
				new $googlemaps.LatLng(47.77348330190455, 8.07271957397461),
				new $googlemaps.LatLng(47.78005882350291, 8.073749542236328),
				new $googlemaps.LatLng(47.788248222487915, 8.068771362304688),
				new $googlemaps.LatLng(47.78617215891291, 8.087482452392578),
				new $googlemaps.LatLng(47.79251542608843, 8.080615997314453),
				new $googlemaps.LatLng(47.79724353976073, 8.064308166503906),
				new $googlemaps.LatLng(47.80254774054839, 8.059329986572266),
				new $googlemaps.LatLng(47.80946544947797, 8.049373626708984),
				new $googlemaps.LatLng(47.817304405844304, 8.042163848876953),
				new $googlemaps.LatLng(47.82894537756019, 8.027915954589844),
				new $googlemaps.LatLng(47.83470726881327, 8.015384674072266),
				new $googlemaps.LatLng(47.83897065647572, 7.999763488769531),
				new $googlemaps.LatLng(47.84519226920408, 7.996845245361328),
				new $googlemaps.LatLng(47.848187594394886, 7.990665435791016),
				new $googlemaps.LatLng(47.855559965615555, 8.004398345947266),
				new $googlemaps.LatLng(47.86108855623189, 7.993068695068359),
				new $googlemaps.LatLng(47.86465879102204, 7.995471954345703),
				new $googlemaps.LatLng(47.866846877903846, 7.973842620849609),
				new $googlemaps.LatLng(47.875943722380235, 7.980709075927734),
				new $googlemaps.LatLng(47.88262140179482, 7.988262176513672),
				new $googlemaps.LatLng(47.88100962694648, 8.004741668701172),
				new $googlemaps.LatLng(47.890219093490316, 8.006629943847656),
				new $googlemaps.LatLng(47.900462700933, 8.017616271972656),
				new $googlemaps.LatLng(47.91243019149635, 8.017101287841797),
				new $googlemaps.LatLng(47.921979184200104, 8.007144927978516),
				new $googlemaps.LatLng(47.933826688535824, 8.01401138305664),
				new $googlemaps.LatLng(47.94682131415562, 8.013496398925781),
				new $googlemaps.LatLng(47.95015569138918, 8.025341033935547),
				new $googlemaps.LatLng(47.95061558860806, 8.033409118652344),
				new $googlemaps.LatLng(47.94406166686348, 8.046627044677734),
				new $googlemaps.LatLng(47.95199525570886, 8.051776885986328),
				new $googlemaps.LatLng(47.96452554509305, 8.04800033569336),
				new $googlemaps.LatLng(47.97843175673324, 8.04473876953125),
				new $googlemaps.LatLng(47.9846366261543, 8.047313690185547),
				new $googlemaps.LatLng(47.984291930752214, 8.057441711425781),
				new $googlemaps.LatLng(47.98095975652571, 8.075122833251953),
				new $googlemaps.LatLng(47.98739410650537, 8.081989288330078),
				new $googlemaps.LatLng(47.996699515093674, 8.07992935180664),
				new $googlemaps.LatLng(48.00359132852724, 8.065166473388672),
				new $googlemaps.LatLng(48.00405075002097, 8.054008483886719),
				new $googlemaps.LatLng(48.008185359331904, 8.032207489013672),
				new $googlemaps.LatLng(48.011400937432974, 8.014183044433594),
				new $googlemaps.LatLng(48.01461631503847, 7.995815277099609),
				new $googlemaps.LatLng(48.02529452322746, 7.981224060058594),
				new $googlemaps.LatLng(48.03562616722427, 7.981052398681641),
				new $googlemaps.LatLng(48.042627548250344, 7.991180419921875),
				new $googlemaps.LatLng(48.05720114142063, 7.993412017822266),
				new $googlemaps.LatLng(48.06695281731474, 8.00130844116211),
				new $googlemaps.LatLng(48.07039414422359, 8.018817901611328),
				new $googlemaps.LatLng(48.06878822030227, 8.031349182128906),
				new $googlemaps.LatLng(48.06683810245475, 8.04250717163086),
				new $googlemaps.LatLng(48.07303233901781, 8.05521011352539),
				new $googlemaps.LatLng(48.07589978864973, 8.065509796142578),
				new $googlemaps.LatLng(48.07222942448527, 8.080101013183594),
				new $googlemaps.LatLng(48.0617903954883, 8.08645248413086),
				new $googlemaps.LatLng(48.04859518737221, 8.095722198486328),
				new $googlemaps.LatLng(48.042512779183404, 8.106708526611328),
				new $googlemaps.LatLng(48.02322794586979, 8.11563491821289),
				new $googlemaps.LatLng(48.01392732242992, 8.123016357421875),
				new $googlemaps.LatLng(48.00600324571969, 8.145503997802734),
				new $googlemaps.LatLng(48.003706184284304, 8.154945373535156),
				new $googlemaps.LatLng(47.998307687244804, 8.16507339477539),
				new $googlemaps.LatLng(47.998307687244804, 8.183097839355469),
				new $googlemaps.LatLng(47.99359789867398, 8.194084167480469),
				new $googlemaps.LatLng(47.99474666723478, 8.206443786621094),
				new $googlemaps.LatLng(47.997963083146544, 8.230476379394531),
				new $googlemaps.LatLng(47.9970441276316, 8.248844146728516),
				new $googlemaps.LatLng(47.99359789867398, 8.264293670654297),
				new $googlemaps.LatLng(47.99646977212298, 8.284549713134766),
				new $googlemaps.LatLng(47.997273868044815, 8.312187194824219),
				new $googlemaps.LatLng(47.99382765443214, 8.319225311279297),
				new $googlemaps.LatLng(47.98670475022854, 8.322315216064453),
				new $googlemaps.LatLng(47.97440968375552, 8.306522369384766),
				new $googlemaps.LatLng(47.96349104914494, 8.302574157714844),
				new $googlemaps.LatLng(47.960847243150745, 8.275623321533203),
				new $googlemaps.LatLng(47.95360482077279, 8.268756866455078),
				new $googlemaps.LatLng(47.952685075446624, 8.303947448730469),
				new $googlemaps.LatLng(47.94452161831068, 8.315448760986328),
				new $googlemaps.LatLng(47.93911693032208, 8.327808380126953),
				new $googlemaps.LatLng(47.93555182685362, 8.353900909423828),
				new $googlemaps.LatLng(47.92658047908093, 8.362998962402344),
				new $googlemaps.LatLng(47.92220925866515, 8.375530242919922),
				new $googlemaps.LatLng(47.916917287080956, 8.38857650756836),
				new $googlemaps.LatLng(47.9092084470098, 8.401966094970703),
				new $googlemaps.LatLng(47.90782763797393, 8.41278076171875),
				new $googlemaps.LatLng(47.901383375369974, 8.41878890991211),
				new $googlemaps.LatLng(47.89666474580635, 8.414325714111328),
				new $googlemaps.LatLng(47.89401752141177, 8.4210205078125),
				new $googlemaps.LatLng(47.888262218857065, 8.42702865600586),
				new $googlemaps.LatLng(47.883887761138745, 8.427715301513672),
				new $googlemaps.LatLng(47.88008859023224, 8.438873291015625),
				new $googlemaps.LatLng(47.87110762412243, 8.440933227539062),
				new $googlemaps.LatLng(47.86258578140335, 8.438358306884766),
				new $googlemaps.LatLng(47.85579033532819, 8.4429931640625),
				new $googlemaps.LatLng(47.850952356425374, 8.452949523925781),
				new $googlemaps.LatLng(47.846229132114566, 8.447799682617188),
				new $googlemaps.LatLng(47.848417996860185, 8.433551788330078),
				new $googlemaps.LatLng(47.85060676923184, 8.421878814697266),
				new $googlemaps.LatLng(47.84703556671536, 8.412094116210938),
				new $googlemaps.LatLng(47.84334890618586, 8.40402603149414),
				new $googlemaps.LatLng(47.83989242396365, 8.397674560546875),
				new $googlemaps.LatLng(47.83747274937861, 8.38531494140625),
				new $googlemaps.LatLng(47.84012286327673, 8.37930679321289),
				new $googlemaps.LatLng(47.837703199439126, 8.370037078857422),
				new $googlemaps.LatLng(47.83897065647572, 8.358707427978516),
				new $googlemaps.LatLng(47.84185112549867, 8.339309692382812),
				new $googlemaps.LatLng(47.843924964165936, 8.334331512451172),
				new $googlemaps.LatLng(47.84334890618586, 8.327808380126953),
				new $googlemaps.LatLng(47.84058373883218, 8.314762115478516),
				new $googlemaps.LatLng(47.84461622529778, 8.301715850830078),
				new $googlemaps.LatLng(47.84830279575547, 8.289871215820312),
				new $googlemaps.LatLng(47.85855469204621, 8.278884887695312),
				new $googlemaps.LatLng(47.863046457371865, 8.269271850585938),
				new $googlemaps.LatLng(47.852968235821955, 8.25897216796875),
				new $googlemaps.LatLng(47.84409778031216, 8.245410919189453),
				new $googlemaps.LatLng(47.83626286976744, 8.242149353027344),
				new $googlemaps.LatLng(47.83269067964535, 8.222064971923828),
				new $googlemaps.LatLng(47.822318411724645, 8.21725845336914),
				new $googlemaps.LatLng(47.816900959034875, 8.205413818359375),
				new $googlemaps.LatLng(47.81205935280115, 8.20404052734375),
				new $googlemaps.LatLng(47.8043349036158, 8.211421966552734),
				new $googlemaps.LatLng(47.797877766184534, 8.217086791992188),
				new $googlemaps.LatLng(47.790843185345324, 8.215885162353516),
				new $googlemaps.LatLng(47.790151207918704, 8.203010559082031),
				new $googlemaps.LatLng(47.784730399308366, 8.195114135742188),
				new $googlemaps.LatLng(47.77850153827617, 8.200263977050781),
				new $googlemaps.LatLng(47.77423318409381, 8.209190368652344),
				new $googlemaps.LatLng(47.767541545369944, 8.207473754882812),
				new $googlemaps.LatLng(47.76211837897506, 8.216743469238281),
				new $googlemaps.LatLng(47.75715626337546, 8.218116760253906),
				new $googlemaps.LatLng(47.75623302686154, 8.203010559082031),
				new $googlemaps.LatLng(47.755309773965905, 8.194942474365234),
				new $googlemaps.LatLng(47.75496354990659, 8.182239532470703),
				new $googlemaps.LatLng(47.75034700898484, 8.180866241455078),
				new $googlemaps.LatLng(47.73903475284852, 8.178462982177734),
				new $googlemaps.LatLng(47.74030461836849, 8.16009521484375),
				new $googlemaps.LatLng(47.742267076885774, 8.149452209472656),
				new $googlemaps.LatLng(47.73568677710032, 8.145675659179688),
				new $googlemaps.LatLng(47.7287592472502, 8.146705627441406),
				new $googlemaps.LatLng(47.72448681114038, 8.14138412475586),
				new $googlemaps.LatLng(47.72044499497374, 8.13589096069336),
				new $googlemaps.LatLng(47.716749345766296, 8.13262939453125),
				new $googlemaps.LatLng(47.7095882794455, 8.129196166992188),
				new $googlemaps.LatLng(47.69953802708595, 8.126106262207031),
				new $googlemaps.LatLng(47.69110356160379, 8.12387466430664),
				new $googlemaps.LatLng(47.68578799221995, 8.12490463256836),
				new $googlemaps.LatLng(47.679431709022325, 8.125247955322266),
				new $googlemaps.LatLng(47.67423053761292, 8.119583129882812),
				new $googlemaps.LatLng(47.66891324873743, 8.11614990234375),
				new $googlemaps.LatLng(47.66347980705349, 8.113574981689453),
				new $googlemaps.LatLng(47.66301736036841, 8.109455108642578)
		    ]];

            var world = new $googlemaps.Polygon({
			    paths: paths,
			    strokeWeight: 0,
			    fillColor: "#444",
			    fillOpacity: 0.3
		    });

/* 		    world.setMap(map);        */
        }
    };


    // Main plugin function
    $.fn.gMap = function (method) {
        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' +  method + ' does not exist on jQuery.gmap');
        }
    };

    // Default settings
    $.fn.gMap.defaults = {
    	baseuri:				 '/lisgmap/getmarkers',
    	baseuri_virtual:		 '/lisgmap/getmarkers-virtual',
    	bounding:				 true,
    	clustering:				 true,
    	spiderfy: 				 true,
    	client:					 'cms',
    	alpmap:					 false,
    	main_design:			 'standard',
        log:                     false,
        address:                 '',
        latitude:                null,
        longitude:               null,
        kml:					 '',
        zoom:                    3,
        maxZoom: 				 null,
        minZoom: 				 null,
        markers:                 [],
        controls:                {},
        scrollwheel:             false,
        maptype:                 google.maps.MapTypeId.TERRAIN,
        maptypeids:              [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.TERRAIN],
        mapTypeControl:          true,
        zoomControl:             true,
        panControl:              false,
        scaleControl:            false,
        streetViewControl:       true,
        singleInfoWindow:        true,

		img_close_icon:			 "/extension/lisgmap/design/standard/images/lisgmap_window_close.png",

        icon: {
            image:               "/extension/lisgmap/design/standard/images/lisgmap_icon_default.png",
            iconsize:            [20, 24],
		    iconanchor:          [10, 24],
		    infowindowanchor:    [130, 100]
        },

        onComplete:              function () {},

        travelMode:              'BYCAR',
        travelUnit:              'KM',
        routeDisplay:            null,
		routeErrors:			 {
                        		    'INVALID_REQUEST': 'The provided request is invalid.',
                                    'NOT_FOUND': 'One or more of the given addresses could not be found.',
                                    'OVER_QUERY_LIMIT': 'A temporary error occured. Please try again in a few minutes.',
                                    'REQUEST_DENIED': 'An error occured. Please contact us.',
                                    'UNKNOWN_ERROR': 'An unknown error occured. Please try again.',
                                    'ZERO_RESULTS': 'No route could be found within the given addresses.'
								 }

    };
}(jQuery));
