
//<![CDATA[
Object.defineProperty( window, 'oax', { writable: false, value: {}} );
Object.defineProperty( window, 'oa', { writable: false, value: oax} );
(function () {
var sA='setAttribute',h=document.head||document.getElementsByTagName('head')[0],qc="cachebust=7c8aaaab",loc=location,oasrv=(loc.search.match(/[\?&]oaserver=([^&]+)/)||[])[1];if (oasrv&&!(/^(?:https?:\/\/)?[a-z]+\.outdooractive\.com$|^127.0.0.1(?::|$)|^localhost(?::|$)/.test(oasrv))) osrv='';
var pr=loc.protocol;if(!/^http/.test(pr))pr='http:';
if(oasrv&&!/^http/.test(oasrv))oasrv=pr+'//'+oasrv.replace(/^[^\/]*\/\//,'');if (typeof oax=='undefined') oax={};
var oru=f('http://www.outdooractive.com/alpportal/oax_head.js?build=mini&excludeCss=true&proj=api-kinzigtal&key=PYAWOSMB-EMWGKPEU-4OSSFEBV&lang=de&callback=initOA')
,jsAbs=f('http://www.outdooractive.com/js');
function f(s){return oasrv?(/\/\//.test(s)?s.replace(/^[^\/]*\/\/[^\/]+/,oasrv):oasrv+s):s.replace(/^[^\/]+\/\//,pr+'//');}
oax._ORIGIN_URL=oru;
oax._CRR_SERVER='https://bgcms.outdooractive.com/';
oax._ORIGIN_BUILD = 'mini';
if (!oax._default_head)
oax._default_head=1;
var base=(function () {
var r=oasrv||'https://www.outdooractive.com'
.replace(/^\w+:/,pr)
.replace(/\/$/,"")
,lH=loc.host
,fu=/^(https?)?\/\//.test(r);
if(fu)r=r.replace(/\/api\//,'/');
if(fu&&0>r.indexOf(lH)&&0>r.indexOf("alpportal"))r+="/alpportal";
return r;
})()
,o=
{
base:base
,css:
[
]
,js:
[




,{code:";(function (){var ac=window.alpConfig||(window.alpConfig={});ac.cachebust='7c8aaaab';ac.cssBase='"+jsAbs+"/';ac.context='/alpportal';ac.language='de';ac.leaflet_gshim=1;ac.mapbox={access_token:'pk.eyJ1Ijoib3V0ZG9vcmFjdGl2ZSIsImEiOiJjamZqZWJoaWM0anNyMnhucnMyMGZkcTVpIn0.hnZInIEEGsjPnXLAvilfhg'};ac.gloadnow=false})();"}
]
,cssLater:
[
]
}
,base=o.base
,lfgm=true;
if (/\bloader\b/.test(loc.search))
 o.js.push({src:jsAbs+'/alp/src_mode_loader.js'});
if (lfgm){
o.js.push({src:jsAbs+'/alp/maps/leaflet_gshim.js'});
}
o.js.push({src:jsAbs+'/oax/api_mini.js'});

var d=document,cE='createElement',sA='setAttribute',fC='firstChild',aC='appendChild',cDF='createDocumentFragment',n,i,v,fr,lk,IE,sc,cba,cb;

IE='onactivate' in d[cE]('div');

setTimeout(jsAsync,100);


function jsAsync() {
fr=d[cDF]();
for (n=o.js.length,i=0;i<n;i++){
v=o.js[i];
if (v){
if (v.src)
{
sc=d[cE]('script');
sc[sA]('src',abs(v.src));
sc[sA]('type','text/javascript');
fr[aC](sc);
}
else
new Function(v.code)();
}}
h[aC](fr);
}

function abs(u){
u=u.replace(/^https?:\/\//,'//');
u=/^\/\//.test(u)?pr+u:base+u.replace(/^([^\/])/,"/$1");
u=u.replace(/\?(cachebust=)?\d+&?/,'?');
var v=u.split('?'),w=v[1]||'';
return v[0]+'?'+qc+(w?'&':'')+w;
}
})();
//]]>
