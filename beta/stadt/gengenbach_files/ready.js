function popup(ziel,w,h) {
	h = h - 20; var x=0, y=0, parameter="";
	if (w < screen.availWidth || h < screen.availHeight) {
		x = (screen.availWidth - w - 12) / 2;
		y = (screen.availHeight - h - 104) / 2;
		if (window.opera) y = 0; // Opera positioniert unter den Symbolleisten
		if (x<0 || y<0) { x=0; y=0; }
	else parameter = "width=" + w + ",height=" + h + ",";
	}
	parameter += "left=" + x + ",top=" + y;
	parameter += ",menubar=0,location=0,toolbar=0,status=0";
	parameter += ",resizable=1,scrollbars=1";
	var Fenster = window.open(ziel,"",parameter);
	if (Fenster) Fenster.focus();
	return !Fenster;
}

$(function() {

	  (function($, undefined) {
  	  // popup on homepage on firstview
  	  if (!localStorage.getItem("popupWasShown")) {
    	  localStorage.setItem("popupWasShown", 1);
        var $popup = $('.kinzigtal #js-popup-home-wrapper');
        if ($popup.length) {
          $.fancybox($popup.html());
        }
      }
	  })(jQuery);

    $('input.button-toubiz-search').live('click', function(){
	    	//toubizid
	    	var search = $('input.toubiz-search').val();

	    	var res = search.substr(0,3);

	    	if(res == 'ID-'){
	    		var search = search.substr(3);
	    	}

	    	if($.isNumeric(search)){

		        newwindow=window.open('http://www3.toubiz.de/kin/default/house.php?id_house='+search, '', 'height=800,width=910,scrollbars=1');
		        if (window.focus) {newwindow.focus()}
		        return false;
	    	}
	    });



$("#img_check").click(function() {
       alert($(this).exifAll());
       //alert("Taken with a " + $(this).exif("Make") + " " + $(this).exif("Model") + " on " + $(this).exif("DateTimeOriginal"));
   });
	// UI TOP
	$().UItoTop({ easingType: 'easeOutQuart' });

	// FANCY
	$("a.fancy").fancybox({
		'titlePosition'  : 'over'
	});

	$("a[rel=fancy]").fancybox({
		'titlePosition'  : 'over'
	});



	// Navigation Overlay
	$(".navi_tooltip").each(function() {
		var content_rel = $(this).attr("rel");
		if ($('#'+content_rel).length > 0) {
			$(this).simpletip({
				fixed:true,
				position:["0", "40"],
				content:$('#'+content_rel).html(),
				showEffect: 'fade',
				hideEffect: 'fade'
			});
		}
	});


	// tabs
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});


});

/*ALLGEMEIN*/
function popup(ziel,w,h) {
  h = h - 20; var x=0, y=0, parameter="";
  if (w < screen.availWidth || h < screen.availHeight) {
    x = (screen.availWidth - w - 12) / 2;
    y = (screen.availHeight - h - 104) / 2;
    if (window.opera) y = 0; // Opera positioniert unter den Symbolleisten
    if (x<0 || y<0) { x=0; y=0; }
    else parameter = "width=" + w + ",height=" + h + ",";
  }
  parameter += "left=" + x + ",top=" + y;
  parameter += ",menubar=0,location=0,toolbar=0,status=0";
  parameter += ",resizable=1,scrollbars=1";
  var Fenster = window.open(ziel,"",parameter);
  if (Fenster) Fenster.focus();
  return !Fenster;
}

function openLink(link) {
  location.href = link;
  return false;
}

function openFormWin() {
    var win = window.open("about:blank", "pop", "width=950, height=900, scrollbars=yes");
};