(function ($) {

  /**
   * The recommended way for producing HTML markup through JavaScript is to write
   * theming functions. These are similiar to the theming functions that you might
   * know from 'phptemplate' (the default PHP templating engine used by most
   * Drupal themes including Omega). JavaScript theme functions accept arguments
   * and can be overriden by sub-themes.
   *
   * In most cases, there is no good reason to NOT wrap your markup producing
   * JavaScript in a theme function.
   */
//  Drupal.theme.prototype.bmhExampleButton = function (path, title) {
//    //Create an anchor element with jQuery.
//    return $('<a href="' + path + '" title="' + title + '">' + title + '</a>');
//  };

  /**
   * Behaviors are Drupal's way of applying JavaScript to a page. In short, the
   * advantage of Behaviors over a simple 'document.ready()' lies in how it
   * interacts with content loaded through Ajax. Opposed to the
   * 'document.ready()' event which is only fired once when the page is
   * initially loaded, behaviors get re-executed whenever something is added to
   * the page through Ajax.
   *
   * You can attach as many behaviors as you wish. In fact, instead of overloading
   * a single behavior with multiple, completely unrelated tasks you should create
   * a separate behavior for every separate task.
   *
   * In most cases, there is no good reason to NOT wrap your JavaScript code in a
   * behavior.
   *
   * @param context
   *   The context for which the behavior is being executed. This is either the
   *   full page or a piece of HTML that was just added through Ajax.
   * @param settings
   *   An array of settings (added through drupal_add_js()). Instead of accessing
   *   Drupal.settings directly you should use this because of potential
   *   modifications made by the Ajax callback that also produced 'context'.
   */
//  Drupal.behaviors.bmhExampleBehavior = {
//    attach: function (context, settings) {
//       // By using the 'context' variable we make sure that our code only runs on
//       // the relevant HTML. Furthermore, by using jQuery.once() we make sure that
//       // we don't run the same piece of code for an HTML snippet that we already
//       // processed previously. By using .once('foo') all processed elements will
//       // get tagged with a 'foo-processed' class, causing all future invocations
//       // of this behavior to ignore them.
//      $('.some-selector', context).once('foo', function () {
//         // Now, we are invoking the previously declared theme function using two
//         // settings as arguments.
//        var $anchor = Drupal.theme('bmhExampleButton', settings.myExampleLinkPath, settings.myExampleLinkTitle);
//
//         // The anchor is then appended to the current element.
//        $anchor.appendTo(this);
//      });
//    }
//  };

  $(window).load(function() {
    Drupal.behaviors.bmhBehaviors.attach(false, Drupal.settings);
  });

  function viewListHeight(row) {
    row.each(function() {
      var leftHeight = $(this).find('.left').outerHeight();
      var rightHeight = $(this).find('.right').outerHeight();

      if(leftHeight > rightHeight) {
        $(this).find('.right').height(leftHeight);
      } else if(rightHeight > leftHeight) {
        $(this).find('.left').height(rightHeight);
      }
    });
  }
  
  function equalKachel(kachel) {
    var kachelHeight = 0;
    kachel.height("auto");
    kachel.each(function() {
      if ($(this).height() >= kachelHeight) {
        kachelHeight = $(this).outerHeight();
      }
    });
    
    kachel.height(kachelHeight);
  }
  
  function customAccordion() {
    $('.cuacc-trigger').not('.active').next('.cuacc-content').hide();
    $('.cuacc-trigger').off();
    $('.cuacc-trigger').on('click', function(e) {
      var trig = $(this);
      
      $('.cuacc-trigger.active').not(trig).next('.cuacc-content').slideToggle('normal');
      $('.cuacc-trigger.active').not(trig).removeClass('active');
      
      trig.next('.cuacc-content').slideToggle('normal');
      trig.toggleClass('active');
      
      return false;
    });
  }
  
  function remove_customAccordion() {
      $('.cuacc-content').show();
      $('.cuacc-trigger').off();
  }
  
  function stickyNav(element) {
      var menuOffset = jQuery(element)[0].offsetTop;
      jQuery(document).bind('ready scroll',function() {
          var docScroll = jQuery(document).scrollTop();
          if(docScroll >= menuOffset) {
              jQuery(element).addClass('fixed');
          } else {
              jQuery(element).removeClass('fixed');
          }
      });
  }

  Drupal.behaviors.bmhBehaviors = {
    attach: function (context, settings) {
        if($('a.openGallery').length > 0) {
            var rel = $('.field--name-field-galerie .colorbox').first().attr('rel');
            $('a.openGallery').click(function(e) {
                e.preventDefault();
                var gallery = $('a[rel='+rel+']').colorbox();
                gallery.eq(0).click();
            });
        }
        
        if($('body').hasClass('omega-mediaqueries-processed')) {
            var omegaSettings = settings.omega || {};
            var mediaQueries = settings.omega.mediaQueries || {};
            
            var currentBreakpoint;
            $.each(mediaQueries, function (index, value) {
              var mql = window.matchMedia(value);
    
              if (mql.matches == true) {
                currentBreakpoint = index;
              }
            });
     
            switch (currentBreakpoint) {
              case 'wide':
              case 'normal':
                viewListHeight($('.view-list .views-row'));
                equalKachel($('.block--nodeblock .node--teaserblock'))
                customAccordion();
                stickyNav('.sticky-wrapper');
                break;
              case 'narrow':
                customAccordion();
                break;
              case 'mobile':
                remove_customAccordion();
                break;
            }
        }
        
        $( ".flex-control-nav" ).wrap( "<div class='flex-control-nav-wrapper'></div>" );

        $('#edit-date-filter-min-datepicker-popup-1').change(function() {
            $('#edit-date-filter-max-datepicker-popup-1').val($('#edit-date-filter-min-datepicker-popup-1').val());
        });
		
		$('#block-nodeblock-4027 .content .field--name-field-link a').attr('target','_blank');
    }
  };

})(jQuery);
;
